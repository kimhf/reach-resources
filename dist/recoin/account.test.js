"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
var const_1 = require("./../const");
var account_1 = require("./account");
it('getDefaultAccountUserMeta() Works!', function () {
    var defaultAccountUserMeta = account_1.getDefaultAccountUserMeta();
    expect(typeof defaultAccountUserMeta.dailyLimitAmount).toBe('number');
    expect(defaultAccountUserMeta.dailyLimitAmount).toBe(const_1.DAILY_RECOIN_HARD_LIMIT);
    expect(typeof defaultAccountUserMeta.bonusDayDate).toBe('string');
    expect(moment(defaultAccountUserMeta.bonusDayDate, 'YYYY-MM-DD').isValid()).toBe(true);
    expect(typeof defaultAccountUserMeta.dailyLimitDate).toBe('string');
    expect(moment(defaultAccountUserMeta.dailyLimitDate, 'YYYY-MM-DD').isValid()).toBe(true);
    expect(typeof defaultAccountUserMeta.bonusDayNum).toBe('number');
});
//# sourceMappingURL=account.test.js.map