"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var const_1 = require("./../const");
function getDefaultAccountUserMeta() {
    var today = new Date();
    var DD = today.getDate();
    var MM = today.getMonth() + 1;
    var YYYY = today.getFullYear();
    var now = YYYY + "-" + MM + "-" + DD;
    var timestamp = Date.now();
    var baseAccount = {
        dailyLimitAmount: const_1.DAILY_RECOIN_HARD_LIMIT,
        dailyLimitDate: now,
        bonusDayNum: 1,
        bonusDayDate: now,
        recommendationWidgetViewNum: 0,
        recommendationWidgetViewTimestamp: timestamp,
        rewardedAerVideoViewTimestamp: timestamp,
        rewardedAerVideoViewNum: 0
    };
    return baseAccount;
}
exports.getDefaultAccountUserMeta = getDefaultAccountUserMeta;
function isInRecomendationWidgetRecoinTimeframe(recoinAccountMeta) {
    var maxViewsTimeframe = const_1.RECOMENDATION_WIDGET_VIEWS_COUNTER_TIMEFRAME;
    var diff = Date.now() - recoinAccountMeta.recommendationWidgetViewTimestamp;
    if (diff <= maxViewsTimeframe) {
        return true;
    }
    return false;
}
exports.isInRecomendationWidgetRecoinTimeframe = isInRecomendationWidgetRecoinTimeframe;
function isUnderRecomendationWidgetRecoinViewlimit(recoinAccountMeta) {
    var maxViewsNum = const_1.RECOMENDATION_WIDGET_VIEWS_COUNTER_LIMIT_NUM;
    if (recoinAccountMeta.recommendationWidgetViewNum < maxViewsNum) {
        return true;
    }
    return false;
}
exports.isUnderRecomendationWidgetRecoinViewlimit = isUnderRecomendationWidgetRecoinViewlimit;
function isInRewardedAerVideoTimeframe(recoinAccountMeta) {
    var maxViewsTimeframe = const_1.REWARDED_VIDEO_VIEWS_COUNTER_TIMEFRAME;
    var diff = Date.now() - recoinAccountMeta.rewardedAerVideoViewTimestamp;
    if (diff <= maxViewsTimeframe) {
        return true;
    }
    return false;
}
exports.isInRewardedAerVideoTimeframe = isInRewardedAerVideoTimeframe;
function isUnderRewardedAerVideoRecoinViewlimit(recoinAccountMeta) {
    var maxViewsNum = const_1.REWARDED_VIDEO_VIEWS_COUNTER_LIMIT_NUM;
    if (recoinAccountMeta.rewardedAerVideoViewNum < maxViewsNum) {
        return true;
    }
    return false;
}
exports.isUnderRewardedAerVideoRecoinViewlimit = isUnderRewardedAerVideoRecoinViewlimit;
//# sourceMappingURL=account.js.map