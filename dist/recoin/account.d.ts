/// <reference types="reach" />
export declare function getDefaultAccountUserMeta(): reach.database.recoin.AccountUserMeta;
export declare function isInRecomendationWidgetRecoinTimeframe(recoinAccountMeta: reach.database.recoin.AccountUserMeta): boolean;
export declare function isUnderRecomendationWidgetRecoinViewlimit(recoinAccountMeta: reach.database.recoin.AccountUserMeta): boolean;
export declare function isInRewardedAerVideoTimeframe(recoinAccountMeta: reach.database.recoin.AccountUserMeta): boolean;
export declare function isUnderRewardedAerVideoRecoinViewlimit(recoinAccountMeta: reach.database.recoin.AccountUserMeta): boolean;
