"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Make sure a error in one promise does not interupt the other promises.
 *
 * @param promises
 */
/*
export async function actuallyPromiseAll (promises: Array<Promise<any>>) {
  let allResults = await Promise.all(promises.map(p => p.catch((e: Error) => e)))
    .then(results => results) // 1,Error: 2,3
    .catch(e => e)

  return allResults
}
*/
/*
export async function actuallyPromiseAll (promises: Array<Promise<any>>) {
  let allResults = await Promise.all(promises.map(p => p.catch((e: Error) => e)))
    .then(results => results) // 1,Error: 2,3
    .catch(e => e)

  return allResults
}
*/
function actuallyPromiseAll(promises) {
    return __awaiter(this, void 0, void 0, function () {
        var _this = this;
        var allResults;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    promises = promises.map(function (p) {
                        if (typeof p.catch !== 'undefined') {
                            return p.catch(function (e) {
                                return e;
                            });
                        }
                        else {
                            var asyncFunc = function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                                var result, e_1;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            _a.trys.push([0, 2, , 3]);
                                            return [4 /*yield*/, p()];
                                        case 1:
                                            result = _a.sent();
                                            resolve(result);
                                            return [3 /*break*/, 3];
                                        case 2:
                                            e_1 = _a.sent();
                                            reject(e_1);
                                            return [3 /*break*/, 3];
                                        case 3: return [2 /*return*/];
                                    }
                                });
                            }); };
                            return new Promise(asyncFunc);
                        }
                    });
                    return [4 /*yield*/, Promise.all(promises)
                            .then(function (results) { return results; })
                            .catch(function (e) { return e; })];
                case 1:
                    allResults = _a.sent();
                    return [2 /*return*/, allResults];
            }
        });
    });
}
exports.actuallyPromiseAll = actuallyPromiseAll;
/**
 * Get firebase cload storage path to the original file from a firebase download url.
 *
 * @param url string The download url to extract path from
 * @return    string The path
 */
function getOriginalStoragePathFromDownloadURL(url) {
    var storagePath = getStoragePathFromDownloadURL(url);
    var regexp = /(?:_crop)?_(?:(?:[0-9]{2,4}x[0-9]{2,4}){1}|(?:xl){1}|(?:lg){1}|(?:sm){1}|(?:md){1}|(?:xs){1})(?:\.[a-z]{3,4})?$/;
    var orgPath = '';
    if (regexp.test(storagePath)) {
        // const regexp = /(?:_crop)?_(([0-9]{2,4}x[0-9]{2,4})|([a-z]{2}))/
        var regexp_1 = /(?:_crop){0,1}_(?:(?:[0-9]{2,4}x[0-9]{2,4}){1}|(?:xl){1}|(?:lg){1}|(?:sm){1}|(?:md){1}|(?:xs){1})/;
        orgPath = storagePath.replace(regexp_1, '');
    }
    else {
        orgPath = storagePath;
    }
    return orgPath;
}
exports.getOriginalStoragePathFromDownloadURL = getOriginalStoragePathFromDownloadURL;
/**
 * Get firebase cload storage path from a firebase download url
 *
 * @param url string The download url to extract path from
 * @return    string The path
 */
function getStoragePathFromDownloadURL(url) {
    var fileName = getFileName(url);
    var storagePath = decodeURIComponent(fileName);
    return storagePath;
}
exports.getStoragePathFromDownloadURL = getStoragePathFromDownloadURL;
/**
 * Get the file name from a url
 *
 * @param urlString
 */
function getFileName(url) {
    if (url) {
        // Remove protocol from url.
        var urlString = url.replace(/(^\w+:|^)\/\//, '');
        // Remove query and hash from url
        urlString = urlString.split('#')[0].split('?')[0];
        if (urlString.indexOf('/') !== -1) {
            var fileName = urlString.split('/').pop();
            if (fileName) {
                return fileName;
            }
        }
    }
    return '';
}
exports.getFileName = getFileName;
/**
 * Makes sure the given string does not contain characters that can't be used as Firebase
 * Realtime Database keys such as '.' and replaces them by '*'.
 *
 * @param key
 */
function makeKeyFirebaseCompatible(key) {
    return key.replace(/\./g, '*');
}
exports.makeKeyFirebaseCompatible = makeKeyFirebaseCompatible;
/**
 * Truncate a string if it is longer than the specified number of characters.
 * Truncated strings will end with a translatable ellipsis sequence ("…") (by default) or specified characters.
 *
 * @see https://www.w3resource.com/javascript-exercises/javascript-string-exercise-16.php
 * @param str
 * @param length
 * @param ending
 */
function textTruncate(str, length, ending) {
    if (length === void 0) { length = 100; }
    if (ending === void 0) { ending = '&hellip;'; }
    if (str.length <= length) {
        return str;
    }
    var subString = str.substr(0, length);
    return subString.substr(0, subString.lastIndexOf(' ')) + ending;
}
exports.textTruncate = textTruncate;
//# sourceMappingURL=helpers.js.map