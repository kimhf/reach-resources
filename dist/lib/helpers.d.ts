/**
 * Make sure a error in one promise does not interupt the other promises.
 *
 * @param promises
 */
export declare function actuallyPromiseAll(promises: any | Array<Promise<any>>): Promise<any>;
/**
 * Get firebase cload storage path to the original file from a firebase download url.
 *
 * @param url string The download url to extract path from
 * @return    string The path
 */
export declare function getOriginalStoragePathFromDownloadURL(url: string): string;
/**
 * Get firebase cload storage path from a firebase download url
 *
 * @param url string The download url to extract path from
 * @return    string The path
 */
export declare function getStoragePathFromDownloadURL(url: string): string;
/**
 * Get the file name from a url
 *
 * @param urlString
 */
export declare function getFileName(url: string): string;
/**
 * Makes sure the given string does not contain characters that can't be used as Firebase
 * Realtime Database keys such as '.' and replaces them by '*'.
 *
 * @param key
 */
export declare function makeKeyFirebaseCompatible(key: string): string;
/**
 * Truncate a string if it is longer than the specified number of characters.
 * Truncated strings will end with a translatable ellipsis sequence ("…") (by default) or specified characters.
 *
 * @see https://www.w3resource.com/javascript-exercises/javascript-string-exercise-16.php
 * @param str
 * @param length
 * @param ending
 */
export declare function textTruncate(str: string, length?: number, ending?: string): string;
