"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var helpers_1 = require("./helpers");
test('getFileName() Get filename from url.', function () {
    expect(helpers_1.getFileName('http://test.com/test/test1.no')).toBe('test1.no');
    expect(helpers_1.getFileName('https://www.test.com/test/test2.no?test=test')).toBe('test2.no');
    expect(helpers_1.getFileName('https://www.test.com/test/test3?test=test&testb=testb')).toBe('test3');
    expect(helpers_1.getFileName('https://firebasestorage.googleapis.com/v0/b/reach-dev-123.appspot.com/o/qweW2%2Ftest7yAE?alt=media&token=123')).toBe('qweW2%2Ftest7yAE');
    expect(helpers_1.getFileName('https://www.test.com')).toBe('');
});
test('getStoragePathFromDownloadURL() Get firebase cload storage path from a firebase download url.', function () {
    expect(helpers_1.getStoragePathFromDownloadURL('https://firebasestorage.googleapis.com/v0/b/reach-dev-14218.appspot.com/o/qweW2%2Ftest7yAE?alt=media&token=123')).toBe('qweW2/test7yAE');
    expect(helpers_1.getStoragePathFromDownloadURL('https://storage.googleapis.com/reach-content/events%2F2458101_1d2pOfvoiEHBUdQn%2Fcover%2Fstock_88987018.0_lg.jpg?GoogleAccessId=firebase-adminsdk-ev5lt@reach-dev-14218.iam.gserviceaccount.com&Expires=16447017600&Signature=LxO36M0u53XLVG3VFXvEtLXKo3hhBW6aOCRi5cF4f0bSVVAzRvvyDCldL84sH%2Fi4lyD9KA4tjawriY%2BBSuRWBv16%2BDDqZCryjKdy38095CO2P8W8jwECt72M9fNGVuDzHwIyiXyiY3MMtBIInhSQuby1Tt5fZtutDq%2Brfh11IXlJGc4pxjnH74JDPLPri1RzlecnW%2FaR5xT2LEWuG1k4LIbKwbBhqufY0Ho81wdybtfp77%2BpXvSv3JMi5zo5S%2FfxXhJS4mndk4nbcaP9dTv8HoPUcDaabzgH5TvHa2zzV%2B15ga2oMBJwVQGVSvIXarP%2B5A0BoDJg%2BhdOk2B9iDcxuA%3D%3D')).toBe('events/2458101_1d2pOfvoiEHBUdQn/cover/stock_88987018.0_lg.jpg');
});
test('getOriginalStoragePathFromDownloadURL() Get firebase cload storage path to the original file from a firebase download url.', function () {
    expect(helpers_1.getOriginalStoragePathFromDownloadURL('https://firebasestorage.googleapis.com/v0/b/reach-dev-14218.appspot.com/o/qweW2%2Ftest7yAE_130x100?alt=media&token=123')).toBe('qweW2/test7yAE');
    expect(helpers_1.getOriginalStoragePathFromDownloadURL('https://firebasestorage.googleapis.com/v0/b/reach-dev-14218.appspot.com/o/qweW2%2Ftest7yAE_crop_130x100?alt=media&token=123')).toBe('qweW2/test7yAE');
    expect(helpers_1.getOriginalStoragePathFromDownloadURL('https://storage.googleapis.com/reach-content/events%2F2458101_1d2pOfvoiEHBUdQn%2Fcover%2Fstock_88987018.0_lg.jpg?GoogleAccessId=firebase-adminsdk-ev5lt@reach-dev-14218.iam.gserviceaccount.com&Expires=16447017600&Signature=LxO36M0u53XLVG3VFXvEtLXKo3hhBW6aOCRi5cF4f0bSVVAzRvvyDCldL84sH%2Fi4lyD9KA4tjawriY%2BBSuRWBv16%2BDDqZCryjKdy38095CO2P8W8jwECt72M9fNGVuDzHwIyiXyiY3MMtBIInhSQuby1Tt5fZtutDq%2Brfh11IXlJGc4pxjnH74JDPLPri1RzlecnW%2FaR5xT2LEWuG1k4LIbKwbBhqufY0Ho81wdybtfp77%2BpXvSv3JMi5zo5S%2FfxXhJS4mndk4nbcaP9dTv8HoPUcDaabzgH5TvHa2zzV%2B15ga2oMBJwVQGVSvIXarP%2B5A0BoDJg%2BhdOk2B9iDcxuA%3D%3D')).toBe('events/2458101_1d2pOfvoiEHBUdQn/cover/stock_88987018.0.jpg');
});
it('actuallyPromiseAll() Handles rejected promises.', function () { return __awaiter(_this, void 0, void 0, function () {
    var promises;
    return __generator(this, function (_a) {
        promises = [];
        promises.push(new Promise(function (_resolve, reject) {
            setTimeout(function () {
                reject("I'm done!");
            }, 10);
        }));
        promises.push(new Promise(function (resolve) {
            setTimeout(function () {
                resolve("I'm done!");
            }, 50);
        }));
        return [2 /*return*/, expect(helpers_1.actuallyPromiseAll(promises)).resolves.toMatchObject(["I'm done!", "I'm done!"])];
    });
}); });
it('actuallyPromiseAll() Handles async functions.', function () { return __awaiter(_this, void 0, void 0, function () {
    var promises, timeoutPromise;
    return __generator(this, function (_a) {
        promises = [];
        timeoutPromise = function (duration) {
            return new Promise(function (resolve) {
                setTimeout(resolve, duration);
            });
        };
        promises.push(function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, timeoutPromise(40)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/, "I'm done!"];
                    }
                });
            });
        });
        promises.push(function () {
            return __awaiter(this, void 0, void 0, function () {
                var e_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _a.trys.push([0, 2, , 3]);
                            return [4 /*yield*/, timeoutPromise(80)];
                        case 1:
                            _a.sent();
                            throw new Error("I'm done!");
                        case 2:
                            e_1 = _a.sent();
                            return [2 /*return*/, e_1.message];
                        case 3: return [2 /*return*/];
                    }
                });
            });
        });
        return [2 /*return*/, expect(helpers_1.actuallyPromiseAll(promises)).resolves.toMatchObject(["I'm done!", "I'm done!"])];
    });
}); });
/**
 * makeKeyFirebaseCompatible() TESTS
 */
it('makeKeyFirebaseCompatible() replace "." with "*"', function () {
    expect(helpers_1.makeKeyFirebaseCompatible('123.qwerty .')).toBe('123*qwerty *');
});
test('makeKeyFirebaseCompatible() Make string compatible as a firebase realtime database key.', function () {
    expect(helpers_1.makeKeyFirebaseCompatible('fed.f2k.dR')).toBe('fed*f2k*dR');
});
/**
 * textTruncate() TESTS
 */
test('textTruncate() Does not break words.', function () {
    expect(helpers_1.textTruncate('Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.', 15, '...')).toBe('Lorem ipsum...');
    expect(helpers_1.textTruncate('Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.', 15, 'X')).toBe('Lorem ipsumX');
});
//# sourceMappingURL=helpers.test.js.map