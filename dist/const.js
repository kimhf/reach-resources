"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DAILY_RECOIN_HARD_LIMIT = 10000;
exports.RECOMENDATION_WIDGET_VIEWS_COUNTER_TIMEFRAME = 12 * 60 * 60 * 1000;
exports.RECOMENDATION_WIDGET_VIEWS_COUNTER_LIMIT_NUM = 3;
exports.REWARDED_VIDEO_VIEWS_COUNTER_TIMEFRAME = 12 * 60 * 60 * 1000;
exports.REWARDED_VIDEO_VIEWS_COUNTER_LIMIT_NUM = 3;
//# sourceMappingURL=const.js.map