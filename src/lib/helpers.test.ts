import {
  getFileName,
  getStoragePathFromDownloadURL,
  getOriginalStoragePathFromDownloadURL,
  actuallyPromiseAll,
  makeKeyFirebaseCompatible,
  textTruncate
} from './helpers'

test('getFileName() Get filename from url.', () => {
  expect(getFileName('http://test.com/test/test1.no')).toBe('test1.no')
  expect(getFileName('https://www.test.com/test/test2.no?test=test')).toBe('test2.no')
  expect(getFileName('https://www.test.com/test/test3?test=test&testb=testb')).toBe('test3')
  expect(getFileName('https://firebasestorage.googleapis.com/v0/b/reach-dev-123.appspot.com/o/qweW2%2Ftest7yAE?alt=media&token=123')).toBe('qweW2%2Ftest7yAE')
  expect(getFileName('https://www.test.com')).toBe('')
})

test('getStoragePathFromDownloadURL() Get firebase cload storage path from a firebase download url.', () => {
  expect(getStoragePathFromDownloadURL('https://firebasestorage.googleapis.com/v0/b/reach-dev-14218.appspot.com/o/qweW2%2Ftest7yAE?alt=media&token=123')).toBe('qweW2/test7yAE')
  expect(getStoragePathFromDownloadURL('https://storage.googleapis.com/reach-content/events%2F2458101_1d2pOfvoiEHBUdQn%2Fcover%2Fstock_88987018.0_lg.jpg?GoogleAccessId=firebase-adminsdk-ev5lt@reach-dev-14218.iam.gserviceaccount.com&Expires=16447017600&Signature=LxO36M0u53XLVG3VFXvEtLXKo3hhBW6aOCRi5cF4f0bSVVAzRvvyDCldL84sH%2Fi4lyD9KA4tjawriY%2BBSuRWBv16%2BDDqZCryjKdy38095CO2P8W8jwECt72M9fNGVuDzHwIyiXyiY3MMtBIInhSQuby1Tt5fZtutDq%2Brfh11IXlJGc4pxjnH74JDPLPri1RzlecnW%2FaR5xT2LEWuG1k4LIbKwbBhqufY0Ho81wdybtfp77%2BpXvSv3JMi5zo5S%2FfxXhJS4mndk4nbcaP9dTv8HoPUcDaabzgH5TvHa2zzV%2B15ga2oMBJwVQGVSvIXarP%2B5A0BoDJg%2BhdOk2B9iDcxuA%3D%3D')).toBe('events/2458101_1d2pOfvoiEHBUdQn/cover/stock_88987018.0_lg.jpg')
})

test('getOriginalStoragePathFromDownloadURL() Get firebase cload storage path to the original file from a firebase download url.', () => {
  expect(getOriginalStoragePathFromDownloadURL('https://firebasestorage.googleapis.com/v0/b/reach-dev-14218.appspot.com/o/qweW2%2Ftest7yAE_130x100?alt=media&token=123')).toBe('qweW2/test7yAE')
  expect(getOriginalStoragePathFromDownloadURL('https://firebasestorage.googleapis.com/v0/b/reach-dev-14218.appspot.com/o/qweW2%2Ftest7yAE_crop_130x100?alt=media&token=123')).toBe('qweW2/test7yAE')
  expect(getOriginalStoragePathFromDownloadURL('https://storage.googleapis.com/reach-content/events%2F2458101_1d2pOfvoiEHBUdQn%2Fcover%2Fstock_88987018.0_lg.jpg?GoogleAccessId=firebase-adminsdk-ev5lt@reach-dev-14218.iam.gserviceaccount.com&Expires=16447017600&Signature=LxO36M0u53XLVG3VFXvEtLXKo3hhBW6aOCRi5cF4f0bSVVAzRvvyDCldL84sH%2Fi4lyD9KA4tjawriY%2BBSuRWBv16%2BDDqZCryjKdy38095CO2P8W8jwECt72M9fNGVuDzHwIyiXyiY3MMtBIInhSQuby1Tt5fZtutDq%2Brfh11IXlJGc4pxjnH74JDPLPri1RzlecnW%2FaR5xT2LEWuG1k4LIbKwbBhqufY0Ho81wdybtfp77%2BpXvSv3JMi5zo5S%2FfxXhJS4mndk4nbcaP9dTv8HoPUcDaabzgH5TvHa2zzV%2B15ga2oMBJwVQGVSvIXarP%2B5A0BoDJg%2BhdOk2B9iDcxuA%3D%3D')).toBe('events/2458101_1d2pOfvoiEHBUdQn/cover/stock_88987018.0.jpg')
})

it('actuallyPromiseAll() Handles rejected promises.', async () => {
  const promises: Array<Promise<any>> = []

  promises.push(new Promise(function (_resolve, reject) {
    setTimeout(() => {
      reject("I'm done!")
    }, 10)
  }))

  promises.push(new Promise(function (resolve) {
    setTimeout(() => {
      resolve("I'm done!")
    }, 50)
  }))

  return expect(actuallyPromiseAll(promises)).resolves.toMatchObject(["I'm done!", "I'm done!"])
})

it('actuallyPromiseAll() Handles async functions.', async () => {
  const promises: Array<Promise<any> | any> = []

  const timeoutPromise = (duration: number) => {
    return new Promise(function (resolve) {
      setTimeout(resolve, duration)
    })
  }

  promises.push(async function (): Promise<any> {
    await timeoutPromise(40)

    return "I'm done!"
  })

  promises.push(async function (): Promise<any> {
    try {
      await timeoutPromise(80)

      throw new Error("I'm done!")
    } catch (e) {
      return e.message
    }
  })

  return expect(actuallyPromiseAll(promises)).resolves.toMatchObject(["I'm done!", "I'm done!"])
})

/**
 * makeKeyFirebaseCompatible() TESTS
 */
it('makeKeyFirebaseCompatible() replace "." with "*"', () => {
  expect(makeKeyFirebaseCompatible('123.qwerty .')).toBe('123*qwerty *')
})
test('makeKeyFirebaseCompatible() Make string compatible as a firebase realtime database key.', () => {
  expect(makeKeyFirebaseCompatible('fed.f2k.dR')).toBe('fed*f2k*dR')
})

/**
 * textTruncate() TESTS
 */
test('textTruncate() Does not break words.', () => {
  expect(textTruncate('Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.', 15, '...')).toBe('Lorem ipsum...')
  expect(textTruncate('Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.', 15, 'X')).toBe('Lorem ipsumX')
})
