/**
 * Make sure a error in one promise does not interupt the other promises.
 *
 * @param promises
 */
/*
export async function actuallyPromiseAll (promises: Array<Promise<any>>) {
  let allResults = await Promise.all(promises.map(p => p.catch((e: Error) => e)))
    .then(results => results) // 1,Error: 2,3
    .catch(e => e)

  return allResults
}
*/
/*
export async function actuallyPromiseAll (promises: Array<Promise<any>>) {
  let allResults = await Promise.all(promises.map(p => p.catch((e: Error) => e)))
    .then(results => results) // 1,Error: 2,3
    .catch(e => e)

  return allResults
}
*/
export async function actuallyPromiseAll (promises: any | Array<Promise<any>>) {
  promises = promises.map((p: Promise<any> | any) => {
    if (typeof p.catch !== 'undefined') {
      return p.catch((e: Error) => {
        return e
      })
    } else {
      const asyncFunc: any = async (resolve: Function, reject: Function) => {
        try {
          const result = await p()
          resolve(result)
        } catch (e) {
          reject(e)
        }
      }
      return new Promise(asyncFunc)
    }
  })

  const allResults = await Promise.all(promises)
    .then(results => results)
    .catch(e => e)

  return allResults
}

/**
 * Get firebase cload storage path to the original file from a firebase download url.
 *
 * @param url string The download url to extract path from
 * @return    string The path
 */
export function getOriginalStoragePathFromDownloadURL (url: string): string {
  const storagePath = getStoragePathFromDownloadURL(url)

  const regexp = /(?:_crop)?_(?:(?:[0-9]{2,4}x[0-9]{2,4}){1}|(?:xl){1}|(?:lg){1}|(?:sm){1}|(?:md){1}|(?:xs){1})(?:\.[a-z]{3,4})?$/
  let orgPath = ''
  if (regexp.test(storagePath)) {
    // const regexp = /(?:_crop)?_(([0-9]{2,4}x[0-9]{2,4})|([a-z]{2}))/
    const regexp = /(?:_crop){0,1}_(?:(?:[0-9]{2,4}x[0-9]{2,4}){1}|(?:xl){1}|(?:lg){1}|(?:sm){1}|(?:md){1}|(?:xs){1})/
    orgPath = storagePath.replace(regexp, '')
  } else {
    orgPath = storagePath
  }

  return orgPath
}

/**
 * Get firebase cload storage path from a firebase download url
 *
 * @param url string The download url to extract path from
 * @return    string The path
 */
export function getStoragePathFromDownloadURL (url: string): string {
  const fileName = getFileName(url)
  const storagePath = decodeURIComponent(fileName)
  return storagePath
}

/**
 * Get the file name from a url
 *
 * @param urlString
 */
export function getFileName (url: string): string {
  if (url) {
    // Remove protocol from url.
    let urlString = url.replace(/(^\w+:|^)\/\//, '')

    // Remove query and hash from url
    urlString = urlString.split('#')[0].split('?')[0]

    if (urlString.indexOf('/') !== -1) {
      let fileName = urlString.split('/').pop()
      if (fileName) {
        return fileName
      }
    }
  }

  return ''
}

/**
 * Makes sure the given string does not contain characters that can't be used as Firebase
 * Realtime Database keys such as '.' and replaces them by '*'.
 *
 * @param key
 */
export function makeKeyFirebaseCompatible (key: string): string {
  return key.replace(/\./g, '*')
}

/**
 * Truncate a string if it is longer than the specified number of characters.
 * Truncated strings will end with a translatable ellipsis sequence ("…") (by default) or specified characters.
 *
 * @see https://www.w3resource.com/javascript-exercises/javascript-string-exercise-16.php
 * @param str
 * @param length
 * @param ending
 */
export function textTruncate (str: string, length: number = 100, ending: string = '&hellip;'): string {
  if (str.length <= length) {
    return str;
  }

  let subString = str.substr(0, length);

  return subString.substr(0, subString.lastIndexOf(' ')) + ending
}
