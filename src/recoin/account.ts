import {
  DAILY_RECOIN_HARD_LIMIT,
  RECOMENDATION_WIDGET_VIEWS_COUNTER_TIMEFRAME,
  RECOMENDATION_WIDGET_VIEWS_COUNTER_LIMIT_NUM,
  REWARDED_VIDEO_VIEWS_COUNTER_TIMEFRAME,
  REWARDED_VIDEO_VIEWS_COUNTER_LIMIT_NUM
} from './../const'

export function getDefaultAccountUserMeta (): reach.database.recoin.AccountUserMeta {
  const today = new Date();
  const DD = today.getDate();
  const MM = today.getMonth()+1;
  const YYYY = today.getFullYear();
  const now = `${YYYY}-${MM}-${DD}`
  const timestamp = Date.now()

  const baseAccount: reach.database.recoin.AccountUserMeta = {
    dailyLimitAmount: DAILY_RECOIN_HARD_LIMIT,
    dailyLimitDate: now,
    bonusDayNum: 1,
    bonusDayDate: now,
    recommendationWidgetViewNum: 0,
    recommendationWidgetViewTimestamp: timestamp,
    rewardedAerVideoViewTimestamp: timestamp,
    rewardedAerVideoViewNum: 0
  }

  return baseAccount
}

export function isInRecomendationWidgetRecoinTimeframe (recoinAccountMeta: reach.database.recoin.AccountUserMeta): boolean {
  const maxViewsTimeframe: number = RECOMENDATION_WIDGET_VIEWS_COUNTER_TIMEFRAME

  const diff: number = Date.now() - recoinAccountMeta.recommendationWidgetViewTimestamp
  if (diff <= maxViewsTimeframe) {
    return true
  }
  return false
}

export function isUnderRecomendationWidgetRecoinViewlimit (recoinAccountMeta: reach.database.recoin.AccountUserMeta): boolean {
  const maxViewsNum: number = RECOMENDATION_WIDGET_VIEWS_COUNTER_LIMIT_NUM

  if (recoinAccountMeta.recommendationWidgetViewNum < maxViewsNum) {
    return true
  }
  return false
}

export function isInRewardedAerVideoTimeframe (recoinAccountMeta: reach.database.recoin.AccountUserMeta): boolean {
  const maxViewsTimeframe: number = REWARDED_VIDEO_VIEWS_COUNTER_TIMEFRAME

  const diff: number = Date.now() - recoinAccountMeta.rewardedAerVideoViewTimestamp
  if (diff <= maxViewsTimeframe) {
    return true
  }
  return false
}

export function isUnderRewardedAerVideoRecoinViewlimit (recoinAccountMeta: reach.database.recoin.AccountUserMeta): boolean {
  const maxViewsNum: number = REWARDED_VIDEO_VIEWS_COUNTER_LIMIT_NUM

  if (recoinAccountMeta.rewardedAerVideoViewNum < maxViewsNum) {
    return true
  }
  return false
}
