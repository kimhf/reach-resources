import * as moment from 'moment'

import {
  DAILY_RECOIN_HARD_LIMIT
} from './../const'

import {
  getDefaultAccountUserMeta
} from './account'

it('getDefaultAccountUserMeta() Works!', () => {
  const defaultAccountUserMeta: reach.database.recoin.AccountUserMeta = getDefaultAccountUserMeta()

  expect(typeof defaultAccountUserMeta.dailyLimitAmount).toBe('number')
  expect(defaultAccountUserMeta.dailyLimitAmount).toBe(DAILY_RECOIN_HARD_LIMIT)

  expect(typeof defaultAccountUserMeta.bonusDayDate).toBe('string')
  expect(moment(defaultAccountUserMeta.bonusDayDate, 'YYYY-MM-DD').isValid()).toBe(true)

  expect(typeof defaultAccountUserMeta.dailyLimitDate).toBe('string')
  expect(moment(defaultAccountUserMeta.dailyLimitDate, 'YYYY-MM-DD').isValid()).toBe(true)

  expect(typeof defaultAccountUserMeta.bonusDayNum).toBe('number')
})
